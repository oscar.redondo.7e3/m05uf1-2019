package com.itb2019;

public class Constants {
    //
    public static final String IDIOMA_CATALA = "ca";
    public static final String IDIOMA_ESPANYOL = "es";
    public static final String IDIOMA_INGLES = "en";

    public static final String ESPAI = " ";
}
